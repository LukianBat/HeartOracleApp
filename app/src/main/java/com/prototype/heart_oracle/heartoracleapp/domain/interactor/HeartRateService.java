package com.prototype.heart_oracle.heartoracleapp.domain.interactor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.prototype.heart_oracle.heartoracleapp.App;

import static android.hardware.SensorManager.SENSOR_STATUS_ACCURACY_HIGH;

public class HeartRateService implements SensorEventListener {
    SensorManager sensorManager;
    Sensor heartRateSensor;
    private HeartRateServiceCallback heartRateServiceCallback;

    public interface HeartRateServiceCallback{
        void getNormalAccuracyCurrent(float current);
    }

    public HeartRateService(){
        sensorManager = (SensorManager) App.getInstance().getSystemService(Context.SENSOR_SERVICE);
        heartRateSensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        sensorManager.registerListener(this, heartRateSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void registerHeartRateServiceCallback(HeartRateServiceCallback heartRateServiceCallback){
        this.heartRateServiceCallback = heartRateServiceCallback;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE && sensorEvent.accuracy==SENSOR_STATUS_ACCURACY_HIGH && sensorEvent.values[0]>0) {
            heartRateServiceCallback.getNormalAccuracyCurrent(sensorEvent.values[0]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
