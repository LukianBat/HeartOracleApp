package com.prototype.heart_oracle.heartoracleapp.domain.interactor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.util.Log;
import com.prototype.heart_oracle.heartoracleapp.App;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.BATTERY_SERVICE;
import static android.provider.Telephony.Carriers.NAME;

public class BluetoothService {
    private BluetoothAdapter bluetoothAdapter;
    private App app;
    private BluetoothServiceCallback bluetoothServiceCallback;
    private PulseServiceCallback pulseServiceCallback;
    private UUID uuid = UUID.fromString("056776ca-8ff1-11e8-9eb6-529269fb1459");
    public interface BluetoothServiceCallback{
        void successConnectingCallingBack();
        void failedConnectingCallingBack();
    }
    public interface PulseServiceCallback{
        void successConnectingCallingBack(BluetoothSocket bluetoothSocket);
        void getMessageCallingBack(String message);
        void successSendingMsgCallingBack();
    }
    public BluetoothService(){
        app = App.getInstance();
    }
    public boolean isEnabled(){
        boolean isEnabled;
        if (app.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)){
            isEnabled = true;
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }else{
            isEnabled = false;
        }
        return isEnabled;
    }
    public void registerBluetoothServiceCallBack(BluetoothServiceCallback callback){
        this.bluetoothServiceCallback = callback;
    }
    public void registerPulseServiceCallBack(PulseServiceCallback callback){
        this.pulseServiceCallback = callback;
    }
    public void turnOn(){
        bluetoothAdapter.enable();
    }
    public void connect() {
        final BluetoothServerSocket serverSocket;
        BluetoothServerSocket presocket = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code.
            presocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, uuid);
            Log.i("TAG", "Socket's listen() method success");
        } catch (IOException e) {
            Log.i("TAG", "Socket's listen() method failed", e);
        }
        serverSocket = presocket;
        Single.create((SingleEmitter<BluetoothSocket> emitter) -> {
            BluetoothSocket socket = null;
            while (true) {
                        try {
                            socket = serverSocket.accept();
                            Log.i("TAG", "Socket's accept() method success");
                        } catch (IOException e) {
                            Log.i("TAG", "Socket's accept() method failed", e);
                            bluetoothServiceCallback.failedConnectingCallingBack();
                            break;
                        }

                        if (socket != null) {
                            // A connection was accepted. Perform work associated with
                            // the connection in a separate thread.
                            try {
                                serverSocket.close();
                                emitter.onSuccess(socket);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                    }
                }
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<BluetoothSocket>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(BluetoothSocket bluetoothSocket) {
                Log.i("TAG", "OnSuccess");
                BatteryManager bm = (BatteryManager)App.getInstance().getSystemService(BATTERY_SERVICE);
                int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                Log.i("TAG", batLevel+"|");
                sendMessage(bluetoothSocket,"Состояние батареи: "+String.valueOf(batLevel));
                bluetoothServiceCallback.successConnectingCallingBack();
                pulseServiceCallback.successConnectingCallingBack(bluetoothSocket);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
    public void sendMessage(BluetoothSocket socket, String msg){
        final OutputStream outputStream;
        OutputStream tmpOut = null;
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("TAG", "Error occurred when creating output stream", e);
        }
        outputStream = tmpOut;
        Single.create(emitter -> {
             try {
                    Log.i("TAG",msg);
                    outputStream.write(msg.getBytes());
                    emitter.onSuccess(msg);
                } catch (IOException e) {
                    Log.i("TAG", "Output stream was disconnected", e);
                }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Object o) {
                pulseServiceCallback.successSendingMsgCallingBack();
            }

            @Override
            public void onError(Throwable e) {

            }
        });


    }
    public void getMessage(BluetoothSocket socket){
        final InputStream inputStream;
        InputStream tmpIn = null;
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            Log.i("TAG", "Error occurred when creating input stream", e);
        }
        inputStream = tmpIn;
        io.reactivex.Observable.create((ObservableOnSubscribe<String>) emitter -> {
            byte[] mmBuffer = new byte[1024];
            int numBytes = 0; // bytes returned from read()
            while (true) {
                try {
                    numBytes = inputStream.read(mmBuffer);
                    if (numBytes>0) {
                        String strmsg = new String(mmBuffer);
                        strmsg = strmsg.substring(0, numBytes);
                        emitter.onNext(strmsg);
                    }
                } catch (IOException e) {
                    Log.i("TAG", "Input stream was disconnected", e);
                    break;
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                Log.i("TAG",s);
                pulseServiceCallback.getMessageCallingBack(s);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.i("TAG","Complete");
            }
        });
    }
}
