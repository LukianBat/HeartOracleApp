package com.prototype.heart_oracle.heartoracleapp.presentation;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.prototype.heart_oracle.heartoracleapp.R;
import com.prototype.heart_oracle.heartoracleapp.domain.MainService;

import androidx.navigation.NavController;

import static androidx.navigation.Navigation.findNavController;

public class ConnectingFragment extends MvpAppCompatFragment implements ConnectingFragmentView{


    private static final int DISCOVERY_REQUEST = 123;
    @InjectPresenter
    ConnectingFragmentPresenter connectingFragmentPresenter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_connecting, container, false);
    }
    @Override
    public void setBTVisibility() {
        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE),
                DISCOVERY_REQUEST);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getContext(),text,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void nextFragment(boolean success) {
        if (success){
            NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
            navController.navigate(R.id.action_connectingFragment_to_successConnectingFragment);
        }
        else {
            getActivity().stopService(new Intent(getActivity(), MainService.class));
            NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
            navController.navigate(R.id.action_connectingFragment_to_failedConnectingFragment);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode>0) {
            getActivity().startService(new Intent(getActivity(), MainService.class));
            connectingFragmentPresenter.connect();
        }
    }

}
