package com.prototype.heart_oracle.heartoracleapp.presentation;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.heart_oracle.heartoracleapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FailedConnectingFragment extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_failed_connecting, container, false);
    }

}
