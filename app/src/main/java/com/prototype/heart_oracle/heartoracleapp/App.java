package com.prototype.heart_oracle.heartoracleapp;

import android.app.Application;

import com.prototype.heart_oracle.heartoracleapp.domain.interactor.BluetoothService;

import androidx.room.Room;

public class App extends Application {

    public static App instance;
    private BluetoothService bluetoothService;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        bluetoothService =  new BluetoothService();
    }

    public static App getInstance() {
        return instance;
    }

    public BluetoothService getBluetoothService() {
        return bluetoothService;
    }
}