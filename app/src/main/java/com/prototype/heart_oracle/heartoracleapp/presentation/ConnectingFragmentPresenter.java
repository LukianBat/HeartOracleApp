package com.prototype.heart_oracle.heartoracleapp.presentation;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.prototype.heart_oracle.heartoracleapp.App;
import com.prototype.heart_oracle.heartoracleapp.domain.interactor.BluetoothService;

@InjectViewState
public class ConnectingFragmentPresenter extends MvpPresenter<ConnectingFragmentView>  implements BluetoothService.BluetoothServiceCallback{
    private BluetoothService bluetoothService;
    ConnectingFragmentPresenter(){
        bluetoothService = App.getInstance().getBluetoothService();
        bluetoothService.registerBluetoothServiceCallBack(this);
        if (bluetoothService.isEnabled()){
            bluetoothService.turnOn();
            getViewState().showToast("Bluetooth module have activated!");
            getViewState().setBTVisibility();
        }else{
            getViewState().showToast("Bluetooth module haven't been detected!");
        }
    }
    public void connect(){
        bluetoothService.connect();
    }

    @Override
    public void successConnectingCallingBack() {
        getViewState().nextFragment(true);
    }

    @Override
    public void failedConnectingCallingBack() {
        getViewState().nextFragment(false);
    }
}