package com.prototype.heart_oracle.heartoracleapp.presentation;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.prototype.heart_oracle.heartoracleapp.R;

import static androidx.navigation.Navigation.findNavController;


public class StartFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
        navController.navigate(R.id.action_startFragment_to_connectingFragment);
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

}
