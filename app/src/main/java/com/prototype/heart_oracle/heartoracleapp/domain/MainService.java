package com.prototype.heart_oracle.heartoracleapp.domain;

import android.app.Service;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;

import com.prototype.heart_oracle.heartoracleapp.App;
import com.prototype.heart_oracle.heartoracleapp.domain.interactor.BluetoothService;
import com.prototype.heart_oracle.heartoracleapp.domain.interactor.HeartRateService;

public class MainService extends Service implements BluetoothService.PulseServiceCallback, HeartRateService.HeartRateServiceCallback {
    private HeartRateService heartRateService;
    private BluetoothSocket bluetoothSocket;
    public void onCreate() {
        App.getInstance().getBluetoothService().registerPulseServiceCallBack(this);
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    void sendCurrent(BluetoothSocket bluetoothSocket, int current) {
        BatteryManager bm = (BatteryManager)App.getInstance().getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        App.getInstance().getBluetoothService().sendMessage(bluetoothSocket, "pulse "+120+" batterylvl "+String.valueOf(batLevel));
    }

    @Override
    public void successConnectingCallingBack(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
        heartRateService = new HeartRateService();
        heartRateService.registerHeartRateServiceCallback(this);
        Log.i("TAG", "Service success connecting");
        App.getInstance().getBluetoothService().getMessage(bluetoothSocket);
    }

    @Override
    public void getMessageCallingBack(String message) {
        Log.i("TAG", "Service got message from bluetooth");
    }

    @Override
    public void successSendingMsgCallingBack() {
        Log.i("TAG", "Success sending message");
    }

    @Override
    public void getNormalAccuracyCurrent(float current) {
        sendCurrent(bluetoothSocket,(int) current);
    }
}
