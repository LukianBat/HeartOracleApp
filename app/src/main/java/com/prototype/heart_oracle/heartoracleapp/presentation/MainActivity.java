package com.prototype.heart_oracle.heartoracleapp.presentation;


import android.os.Bundle;
import com.prototype.heart_oracle.heartoracleapp.R;


import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Navigation.findNavController(this, R.id.my_nav_host_fragment);
    }

}
