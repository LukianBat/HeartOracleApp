package com.prototype.heart_oracle.heartoracleapp.presentation;

import android.view.View;

import com.arellomobile.mvp.MvpView;

public interface ConnectingFragmentView extends MvpView {
    void setBTVisibility();
    void showToast(String text);
    void nextFragment(boolean success);
}
